const Binance = require('binance-api-node').default;
const _ = require('lodash');
const sendgrid = require('@sendgrid/mail');
const TableBuilder = require('table-builder');
const inlineCss = require('inline-css');
const axios = require('axios');
const config = require('./config');
const fs = require('fs');
require.extensions['.css'] = function (module, filename) {
  module.exports = fs.readFileSync(filename, 'utf8');
};
const styles = require('./style.css');

sendgrid.setApiKey(config.sendgrid_api_key);

const client = Binance({
  apiKey: config.binance_api_key,
  apiSecret: config.binance_api_secret,
});

const table = [];
const tableHeaders = {
  asset: 'Asset',
  quantity: 'Quantity',
  cost: 'Cost (ETH)',
  change_all: 'Change (all-time)',
  change_24: 'Change (24 hours)',
  amount_eth: 'Amount (ETH)',
  amount_usd: 'Amount (USD)',
  amount_aud: 'Amount (AUD)',
};
const totalTable = [];
const totalTableHeaders = {
  total_eth: 'Total (ETH)',
  total_usd: 'Total (USD)',
  total_aud: 'Total (AUD)',
};

const currencyConversionEndpoint = 'https://api.fixer.io/latest?base=USD&symbols=AUD';

let amounts = [];
let ticker = null;
const mainCoin = 'ETH';
let totalUSD = 0;
let totalCoin = 0;
let audRate = 0;

axios.get(currencyConversionEndpoint)
  .then((response) => {
    audRate = _.get(response, 'data.rates.AUD', 0);
  })
  .then(client.accountInfo)
  .then((accountInfo) => {
    amounts = accountInfo.balances
      .filter(balance => parseFloat(balance.free) > 0)
      .map(balance => ({
        asset: balance.asset,
        free: parseFloat(balance.free),
        trades: [],
      }));
  })
  .then(client.prices)
  .then((res) => {
    ticker = res;
  })
  .then(() => {
    // Get the current price for the asset from the ticker
    amounts.forEach((amount, index) => {
      amounts[index]['price'] = parseFloat(ticker[`${amount.asset}${mainCoin}`]);
    });
    amounts = amounts.filter(amount => !isNaN(amount.price));
  })
  .then(() => {
    // Fetch trades for each amount
    const promises = [];
    amounts.forEach((amount, index) => {
      promises.push(
        client
          .myTrades({symbol: `${amount.asset}${mainCoin}`})
          .then((trades) => {
            amounts[index]['trades'] = trades;
          })
      );
    });
    return Promise.all(promises);
  })
  .then(() => {
    // Run through the trade history
    amounts.forEach((amount, index) => {
      const orderedTrades = _.orderBy(amount.trades, ['time'], ['asc']);
      let qty = 0;
      let cost = 0;
      orderedTrades.forEach((trade) => {
        const q = parseFloat(trade.qty);
        const p = parseFloat(trade.price);
        const total = q * p;
        qty = qty + (!trade.isBuyer ? -q : q);
        cost = cost + (!trade.isBuyer ? -total : total);
      });
      amounts[index]['qty'] = qty;
      amounts[index]['cost'] = cost;
    });
  })
  .then(() => {
    // Calculate the percentage change
    amounts.forEach((amount, index) => {
      const currentCost = amount.qty * amount.price;
      amounts[index]['diff'] = ((currentCost - amount.cost) / currentCost * 100);
    });
  })
  .then(() => {
    // Fetch 24 hour history for each amount
    const promises = [];
    amounts.forEach((amount, index) => {
      promises.push(
        client
          .dailyStats({symbol: `${amount.asset}${mainCoin}`})
          .then((stats) => {
            amounts[index]['stats'] = stats;
          })
      );
    });
    return Promise.all(promises);
  })
  .then(() => {
    // Calculate USD totals
    amounts.forEach((amount, index) => {
      const coin = amount.price * amount.free;
      const usd = parseFloat(ticker[`${mainCoin}USDT`]) * coin;
      amounts[index]['usd'] = usd.toFixed(2);
      totalUSD += usd;
      totalCoin += coin;
    });
  })
  .then(() => {
    amounts.forEach((amount) => {
      table.push({
        asset: amount.asset,
        quantity: amount.free,
        cost: amount.cost.toFixed(8),
        change_all: `${amount.diff.toFixed(3)}%`,
        change_24: `${amount.stats.priceChangePercent}%`,
        amount_eth: (amount.free * amount.price).toFixed(8),
        amount_usd: `$${amount.usd}`,
        amount_aud: `$${(amount.usd * audRate).toFixed(2)}`,
      });
    });
    totalTable.push({
      total_eth: totalCoin.toFixed(8),
      total_usd: `$${totalUSD.toFixed(2)}`,
      total_aud: `$${(totalUSD * audRate).toFixed(2)}`,
    });
  })
  .then(() => {
    const tableRendered = new TableBuilder()
      .setHeaders(tableHeaders)
      .setData(table)
      .render();
    const totalTableRendered = new TableBuilder()
      .setHeaders(totalTableHeaders)
      .setData(totalTable)
      .render();

    const html = `<style>${styles}</style>${tableRendered}<br>${totalTableRendered}`;

    inlineCss(html, {url: '/'})
      .then((html) => {
        const msg = {
          to: config.target_email,
          from: config.target_email,
          subject: 'Binance Update',
          text: html,
          html,
        };
        sendgrid.send(msg);
      });
  });